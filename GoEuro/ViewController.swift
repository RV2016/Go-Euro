//
//  ViewController.swift
//  GoEuro
//
//  Created by Rachit on 15/01/17.
//  Copyright © 2017 Rachit Vyas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GEPageVCDelegate {
    
    
    @IBOutlet weak var vwSegmentContainr: UIView!
    
    @IBOutlet weak var vwContainer: UIView!
    
    private var  segmented : GESegmentControl?
    
    private var selectedIndex : Int = 0
    
    private var aPageVC : GEPageVC? = nil
    
    private var willMoveToIndex : Int = -99
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if self.segmented == nil{
            
            segmented = GESegmentControl(
                frame: CGRect(
                    x: 0,
                    y: 0,
                    width: view.frame.size.width,
                    height: 44),
                titles: [
                    "TRAIN",
                    "BUS",
                    "FLIGHT"
                ],
                action: {
                    control, index in
                    
                    print ("segmented did pressed \(index)")
                    
                    let directionIsForward : Bool  = index > self.selectedIndex ? true : false
                    
                    self.selectedIndex = index
                    
                    self.aPageVC?.changeScreen(self.selectedIndex,forward: directionIsForward)
                    
//                    self.performSegueWithIdentifier("embedpagevc", sender: self)
            })
            
            segmented!.appearance.backgroundColor  = UIColor.blueColor()
            segmented!.appearance.textColor = UIColor.whiteColor()
            segmented!.appearance.selectorColor = UIColor.yellowColor()
            segmented!.appearance.selectedTextColor = UIColor.yellowColor()
            self.vwSegmentContainr.addSubview(segmented!)


        }
        
        print(self.vwContainer.subviews)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:     -   Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "embedpagevc" {
            
            if let aPageVC : GEPageVC? = segue.destinationViewController as? GEPageVC{
                
               // aPageVC?.changeScreen(self.selectedIndex)
                
                self.aPageVC = aPageVC!
                
                self.aPageVC?._delegate = self
            }
            else{
                
                print("Can't reach to GEPageVC")
            }
            
            
        }
    }
    
    
    //MARK:     -   PageVC Delegate Methods
    
    func willTransit(toIndex : Int){
        
        print("willTransit to index : \(toIndex)")
        
        self.willMoveToIndex = toIndex
    }
    
    func didTransit(fromIndex : Int, completed : Bool){
        
        print("didTransit from index : \(fromIndex) :  completed : \(completed)")
        
        if completed == true && self.willMoveToIndex != -99{
            
             self.segmented?.selectItemAtIndex(self.willMoveToIndex, withAnimation: true)
        }
        else{
            
            self.willMoveToIndex = -99
        }
    }

    
    


}

