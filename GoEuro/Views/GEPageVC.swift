//
//  GEPageVC.swift
//  GoEuro
//
//  Created by Rachit on 15/01/17.
//  Copyright © 2017 Rachit Vyas. All rights reserved.
//

import UIKit

@objc protocol GEPageVCDelegate {
    
    optional func willTransit(toIndex : Int)
    optional func didTransit(fromIndex : Int, completed : Bool)
}

class GEPageVC: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    weak var _delegate : GEPageVCDelegate? = nil
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.generateNewListVC("TrainsContentVC"),
                self.generateNewListVC("BusContentVC"),
                self.generateNewListVC("FlightContentVC")]
    }()
    
    private func generateNewListVC(storyboardID: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewControllerWithIdentifier("\(storyboardID)")
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .Forward,
                               animated: true,
                               completion: nil)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK:    -   Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK:    -   Page Controller DataSource
    
    //extension GEPageVC: UIPageViewControllerDataSource {
        
        func pageViewController(pageViewController: UIPageViewController,
                                viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.indexOf(viewController) else {
                return nil
            }
            
            let previousIndex = viewControllerIndex - 1
            
            guard previousIndex >= 0 else {
                return nil
            }
            
            guard orderedViewControllers.count > previousIndex else {
                return nil
            }
            
            return orderedViewControllers[previousIndex]
        }
        
        func pageViewController(pageViewController: UIPageViewController,
                                viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.indexOf(viewController) else {
                return nil
            }
            
            let nextIndex = viewControllerIndex + 1
            let orderedViewControllersCount = orderedViewControllers.count
            
            guard orderedViewControllersCount != nextIndex else {
                return nil
            }
            
            guard orderedViewControllersCount > nextIndex else {
                return nil
            }
            
            return orderedViewControllers[nextIndex]
        }
    
    
  
    
    
    // MARK:    -   Page Controller Delegate
    
    func changeScreen(nextScreenIndex : Int, forward:Bool){
        
        print("changeScreen() -- nextScreenIndex -> \(nextScreenIndex)")
        
         var aDirection  : UIPageViewControllerNavigationDirection =  .Forward
        
        if forward == false {
            
            aDirection = .Reverse
        }
        
    
        if let firstViewController : UIViewController = orderedViewControllers[nextScreenIndex] {
            setViewControllers([firstViewController],
                               direction:aDirection,
                               animated: true,
                               completion: nil)
        }
    
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]){
        
//        print(pageViewController)
//        print(pendingViewControllers)
//        
//        print(self.orderedViewControllers.indexOf(pendingViewControllers[0]))
        
        if self._delegate != nil {
            
            _delegate?.willTransit!(self.orderedViewControllers.indexOf(pendingViewControllers[0])!)
            
    
        }
        
        
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool){
        
//        print("animation finished \(finished) at \(NSDate())")
//        print("previousViewControllers \(previousViewControllers.count) ")
//        print(self.orderedViewControllers.indexOf(previousViewControllers[0])!)
//        print("transition Completed \(completed) at \(NSDate())")
        
        if self._delegate != nil {
            
            _delegate?.didTransit!(self.orderedViewControllers.indexOf(previousViewControllers[0])!, completed: completed)
        }
    }
        
    //}

}
