//
//  GEListVC.swift
//  GoEuro
//
//  Created by Rachit on 15/01/17.
//  Copyright © 2017 Rachit Vyas. All rights reserved.
//

import UIKit

class GEListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:     -   IBOutlets
    
    @IBOutlet weak var tblResult: UITableView!
    
    
    
    //MARK:     -   Vars
    
    
    
    //MARK:     -   View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblResult.delegate = self
        self.tblResult.dataSource = self
        
        self.tblResult.rowHeight = UITableViewAutomaticDimension
        
         self.tblResult.estimatedRowHeight = 120.0
        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
         self.tblResult.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        
        //self.tblResult.hidden  = true
        
        self.tblResult.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: -   Tableview DataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }

    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellResult :  GEResultListCell? =   tableView.dequeueReusableCellWithIdentifier("GEResultListCell", forIndexPath: indexPath) as? GEResultListCell

        cellResult?.ivProviderLogo.image = UIImage(named: "placeholder-logo.png")
        cellResult?.lblPrice1.text  = "€ 19"
        cellResult?.lblPrice2.text  = ".99"
        cellResult?.lblTime.text  = "hh:mm - hh:mm"
        cellResult?.lblStops.text  = "Direct"
        cellResult?.lblDuration.text  = "hh:mm h"
        
        
        return cellResult!
    }
    
    
    

}
