//
//  GEResultListCell.swift
//  GoEuro
//
//  Created by Rachit on 15/01/17.
//  Copyright © 2017 Rachit Vyas. All rights reserved.
//

import UIKit

class GEResultListCell: UITableViewCell {
    
    
    //MARK: -   IBoutLets
    
    @IBOutlet weak var ivProviderLogo: UIImageView!
    
    @IBOutlet weak var lblPrice1: UILabel! // e.g. € 19
    
    @IBOutlet weak var lblPrice2: UILabel! // e.g. .99
    
    @IBOutlet weak var lblTime: UILabel! // may be departure or arrival format -- "hh:mm - hh:mm" -- no preceding ZERO
    
    @IBOutlet weak var lblStops: UILabel! // e.g. "Direct" or "n Stops"
    
    @IBOutlet weak var lblDuration: UILabel! // e.g. "hh:mm h"
    @IBOutlet weak var ivDisclosure: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
